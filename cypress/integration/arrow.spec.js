it('nada agora', function(){})

// function soma(a,b){
//   return a + b;
// }

//função anonima
// const soma = function (a, b){
//   return a + b;
// }

//arrow function - forma 1 - retorno explícito
// const soma = (a,b) => {
//   return a + b;
// } 

//arrow function - forma 2 - retorno implícito
// const soma = (a,b) => a+b

//arrow function - forma 3 - somente um parâmetro [pode-se retirar os parenteses]
//const soma = a => a+a

//arrow function - forma 4 - sem parâmetros [é necessário passar os parenteses]
// const soma = () => 5+4
// console.log(soma());

//console.log(soma(3, 3));

it('a function test...', function(){
  console.log('Function', this);
})

it('an arrow test...', () => {
  console.log('Arrow', this);
})

